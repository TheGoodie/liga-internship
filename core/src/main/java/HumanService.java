import java.util.ArrayList;
import java.util.Arrays;

public class HumanService implements Service{
    @Override
    public boolean match() {
        return false;
    }

    public boolean match(ArrayList<Human> people, Human human)
    {
        if(people.contains(human)){
            return true;
        }
        return false;
    }

    @Override
    public void find() {

    }
    public void find(ArrayList<Human> people)
    {
        ArrayList<Character> symbols = new ArrayList(Arrays.asList('а','б','в','г','д','А', 'Б','В','Г','Д'));
        for (Human i : people){
            if(i.getAge() < 20 && symbols.contains(i.getSurname().charAt(0))){
                System.out.println(i);
            }
        }
    }
}
