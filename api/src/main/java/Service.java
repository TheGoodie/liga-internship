import java.util.ArrayList;

public interface Service {
    boolean match();
    void find();
}
