import java.util.Date;

public class Human {
    private String name;
    private String surname;
    private String middleName;
    private int age;
    private boolean gender;
    private Date birthday;

    Human(String name, String surname, String middleName, int age, Date birthday) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.age = age;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}
    public Date getBirthday() {return birthday;}
    public void setBirthday(Date birthday) {this.birthday = birthday;}
}
